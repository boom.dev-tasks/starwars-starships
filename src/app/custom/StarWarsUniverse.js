import Starship from './Starship';

export default class StarWarsUniverse {
    constructor() {
        this.starships = [];
    }

    get theBestStarship() {
        let currentBestStarship = this.starships[0]
        this.starships.forEach(starship => {
            if (starship.maxDaysInSpace > currentBestStarship.maxDaysInSpace) {
                currentBestStarship = starship;
            }
        });

        return currentBestStarship;
    }

    async init() {
        const starshipsCount = await this._getStarshipCount();
        await this._createStarships(starshipsCount);
    }

    async _createStarships(starshipsCount) {

        for (let i = 1; i <= starshipsCount; i++) {
            const API_URL = `https://swapi.boom.dev/api/starships/${i}`;

            // const nums = [1, 4, 6, 7, 8, 14, 16, 18, 19, 20, 24, 25, 26, 30, 33, 34, 35, 36]

            // if (!nums.includes(i)) {
            //     var responce = await fetch(API_URL).then(r => r.json());
            //     const { name, consumables, passengers } = responce;

            //     if (this._validateData(consumables, passengers)) {
            //         let starship = new Starship(name, this._consumablesParser(consumables),
            //             this._passengersParser(passengers));
            //         this.starships.push(starship);
            //     }
            // }
            await fetch(API_URL)
                .then(async (data) => {
                    if (data.ok) {
                        var responce = await data.json()
                        const { name, consumables, passengers } = responce;

                        if (this._validateData(consumables, passengers)) {
                            let starship = new Starship(name, this._consumablesParser(consumables),
                                this._passengersParser(passengers));
                            this.starships.push(starship);
                        }
                    }
                })
        }
    }

    _passengersParser(passengers) {
        const pCleaned = passengers.replace(',', '');
        return parseInt(pCleaned);
    }

    _consumablesParser(consumables) { //parse consumables to days
        const date = consumables.split(" ");
        const number = parseInt(date[0]);

        switch (date[1]) {
            case "year":
            case "years":
                var days = number * 365;
                return days;

            case "month":
            case "months":
                var days = number * 30;
                return days;

            case "week":
            case "weeks":
                var days = number * 7;
                return days;
        }
    }

    _validateData(consumables, passengers) {
        if (consumables !== undefined && consumables !== null && consumables !== 'unknown' &&
            passengers !== undefined && passengers !== null && passengers !== 'n/a'
            && passengers !== '0') {
            return true;
        }

        return false;
    }

    async _getStarshipCount() {
        const API_URL = 'https://swapi.boom.dev/api/starships/';
        const { count } = await fetch(API_URL).then(r => r.json());
        return count;
    };
}